# filter-calendar

A project to filter events from a given calendar.  It filters a set of regular expressions defined on `regexes`.

# Dependencies

The parsing of the calendar is done through `icalendar` library.  Hence, it should be installed:

    pip install -r requirements.txt 

## To do

- Add arguments instead of hard coded variables (I am lazy)