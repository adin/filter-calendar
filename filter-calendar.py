#!/usr/bin/env python3

from datetime import datetime, timedelta, timezone
import icalendar
from dateutil.rrule import *
import re


inputFile = '/tmp/calendar.ics'
outputFile = inputFile.replace('.ics','.new.ics')

regexes = ["Início das aulas do .+ período", "Último dia para o cumprimento", "Não haverá atividades"]

icalfile = open(inputFile, 'rb')
gcal = icalendar.Calendar.from_ical(icalfile.read())
cal = icalendar.Calendar()
for component in gcal.walk():
    if component.name == "VEVENT" and any(re.match(exp, component.get('description')) for exp in regexes):
        cal.add_component(component)
icalfile.close()
with open(outputFile, 'wb') as f:
    f.write(cal.to_ical())
